# IMPACTO DEL CORONAVIRUS EN LAS REDES SOCIALES

**AUTORES**

- Juan Ignacio Casas González 100364563
- Nieves Montero Fernández 100364511
- Luisa Xue Xia 100364494

**HERRAMIENTAS UTILIZADAS**
- Cuenta de desarrollador de Twitter
- Programa para desarrollar el código: Eclipse
- API que analiza los sentimientos o connotación de los tweets: MonkeyLearn

**PROPÓSITO DEL PROYECTO**

Nuestro principal objetivo es analizar hasta qué punto el Covid-19 ha supuesto un impacto en el día día de los ciudadanos. Hemos decidido analizarlo a través de la red social Twitter, que es una de las más multitudinarias y en las que más opiniones de diferentes grupos sociales se recopila.
Con el paso de los meses, nos hemos dado cuenta de que el Covid-19 se ha convertido en el tema por excelencia de conversación. Y es que, desde marzo, nuestras vidas han dado un giro de 180º tras el comienzo de la pandemia y, aparentemente, ya no sabemos hablar de otra cosa. Por ello, hemos querido analizar cuáles son las opiniones y sensaciones que ha generado esta situación, a través de la red social que más opiniones variadas acumula a lo largo del día.

**DESARROLLO Y RESULTADOS**

El desarrollo de nuestro proyecto consistirá en recopilar y analizar tweets que contienen el hashtag #Covid-19. Se han recopilado tweets en distintos instantes de tiempo:


![](/Tabla_de_tweets.jpeg)

![](/Tipos_de_tweets.jpeg)


**CONCLUSIONES**

Tras realizar una recopilación de ficheros durante cinco días consecutivos, hemos podido determinar que la mayor parte de los tweets son de carácter neutro, con un porcentaje aproximado del 80.95%. A continuación, encontramos los tweets negativos con un porcentaje de aproximadamente 15.81%. Y finalmente, tenemos los tweets positivos, los menos multitudinarios de todos, con un porcentaje de 3.22%.
Tras analizar los anteriores porcentajes, podemos llegar a la conclusión de que, en su mayoría, la comunidad de Twitter habla del Covid-19 desde un punto de vista más informativo y académico, que desde una perspectiva personal. No obstante, cuando se trata de perspectivas personales, podemos observar que la mayor parte se tratan de opiniones negativas.
