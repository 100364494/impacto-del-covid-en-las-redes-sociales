package cdist;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import com.vdurmont.emoji.EmojiParser;
import com.monkeylearn.MonkeyLearn;
import com.monkeylearn.MonkeyLearnResponse;
import com.monkeylearn.MonkeyLearnException;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterProducer implements StatusListener {

	public static final String _consumerKey = "Fx9DoUdiDUYsTCL8slJ2UI9AW";
	public static final String _consumerSecret = "CfouqzRxwI4rcadiSwPP0ygVnsPODaSSGlZWqcCRJaJevXT58v";
	public static final String _accessToken = "436124192-WqGFw8akAFO29KE9MTp3PJAQHB1LtyxZXaiD0rEK";
	public static final String _accessTokenSecret = "KUS5UOiwYys9SLWxXjMygfF4tsXHs7uckNpCDgYpnPGd8";
	private static final boolean EXTENDED_TWITTER_MODE = true;

	static File file = null;
	static PrintWriter print = null;
	
	static int count = 0;
	static int positive = 0;
	static int negative = 0;
	static int neutral = 0;

	private static void createShutDownHook(String path, PrintWriter pr) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("\nAgregando resumen al fichero " + path);
				
				//Una vez hemos terminado el streaming de Tweets, almacenamos en un fichero llamado Covid19.txt los resultados obtenidos.
				
				pr.println("\n");
				pr.println("Total " + count);
				pr.println("Positivos: " + positive + ". " + (positive*100.0/count) + "%");
				pr.println("Neutros: " + neutral + ". " + (neutral*100.0/count) + "%");
				pr.println("Negativos: " + negative + ". " + (negative*100.0/count) + "%");
				pr.flush();
				pr.close();
				System.out.println("Ha finalizado el proceso de descarga de Tweets. Acceda al fichero Covid19.txt para observar el resumen.");
			}
		}));
	}

	/* Cuando llega un nuevo tweet */
	public void onStatus(Status status) {

		String text = status.getText();

		//Depuramos el string para enviarlo al API de analisis de sentimiento
		String filteredTweet = EmojiParser.removeAllEmojis(text.replace("\n", "").replace("\r", ""));

		/* Realizamos el analisis de sentimientos para el tweet,
		 * sumando a las variables globales el resultado de este analisis.*/
		try {

			MonkeyLearn ml = new MonkeyLearn("97031b1826bd3cac02bed65dc53ba52be451b3c6");
			 
	        String moduleId = "cl_pi3C7JiL";
	        String[] data = {filteredTweet};
	        
	        MonkeyLearnResponse res = ml.classifiers.classify(moduleId, data, false);
			String sentiment = res.arrayResult.toString();
			
			if(sentiment.contains("Positive")){
				positive++;
			}else if(sentiment.contains("Neutral")){
				neutral++;
			} else{
				negative++;
			}

			print.println(filteredTweet);
			System.out.println("Nuevo Tweet descargado");
			count++;

		} catch (MonkeyLearnException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void onException(Exception arg0) {
		System.out.println("Exception on twitter");

	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		System.out.println("Exception on twitter");

	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		System.out.println("onScrubGeo");

	}

	@Override
	public void onStallWarning(StallWarning arg0) {
		System.out.println("EonStallWarning");

	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		System.out.println("EonTrackLimitationNotice");

	}

	public TwitterProducer(String simplequery, String filepath) throws InterruptedException {
		super();
		
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		
		configurationBuilder.setOAuthConsumerKey(_consumerKey).setOAuthConsumerSecret(_consumerSecret)
				.setOAuthAccessToken(_accessToken).setOAuthAccessTokenSecret(_accessTokenSecret);
		
		TwitterStream twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
		twitterStream.addListener(this);
		FilterQuery tweetFilterQuery = new FilterQuery();
		tweetFilterQuery.track(new String[] { simplequery });
		twitterStream.filter(tweetFilterQuery);

		try {
			file = new File(filepath);
			print = new PrintWriter(file);

		} catch (IOException e) {
			System.err.print("Problem with file " + e.getMessage());
			System.exit(1);
		}


		Thread.sleep(30000);

		createShutDownHook(filepath, print);
		System.exit(1);

	}

	public static void main(String[] args) throws InterruptedException {

		String hashtag = "Covid-19";
		String fichero = "Covid19.txt";
		


		TwitterProducer tp = new TwitterProducer(hashtag, fichero);

	}

}
